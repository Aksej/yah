;;;; package.lisp

(mgl-pax:define-package #:yah
    (:use #:cl #:mgl-pax)
  (:export #:finger #:heap #:make-heap #:insert! #:extract! #:insert-extract!
           #:extract-insert! #:peek #:change-key! #:empty-heap? #:delete!))

(in-package #:yah)

(defsection @yah (:title "YAH - Yet Another Heap")
  (yah asdf/system:system)
  (finger class)
  (heap class)
  (make-heap function)
  (insert! function)
  (extract! function)
  (insert-extract! function)
  (extract-insert! function)
  (peek function)
  (change-key! function)
  (delete! function)
  (empty-heap? function))

(defun write-readme (&optional
                       (file "~/quicklisp/local-projects/yah/README.md"))
  (with-open-file (stream file
                          :direction :output
                          :if-exists :supersede
                          :if-does-not-exist :create)
    (document @yah
              :stream stream)))
