# YAH - Yet Another Heap

###### \[in package YAH\]
## YAH ASDF System

- Version: 0.0.1
- Description: Yet Another Heap
- Licence: BSD-3
- Author: Thomas Bartscher <thomas-bartscher@weltraumschlangen.de>


- [class] FINGER

- [class] HEAP

- [function] MAKE-HEAP PREDICATE

    Makes a heap sorting by predicate `PREDICATE`. This means that if there are
    two values with keys A and B on the heap, if `(funcall predicate a b)` is
    true, A will be popped off the heap before B.

- [function] INSERT! HEAP KEY VALUE

    Add `VALUE` with at `KEY` into the `HEAP`. Returns a finger to the resulting
    entry in the heap.

- [function] EXTRACT! HEAP

    Pop the top element from the `HEAP`, according to the heap's
    predicate. Returns two values: The value and the key of the popped element.
    
    Fails if `HEAP` is empty.

- [function] INSERT-EXTRACT! HEAP KEY VALUE

    Insert `VALUE` at `KEY` into `HEAP`, then immediately pop the top element.

- [function] EXTRACT-INSERT! HEAP KEY VALUE

    First pop the top element of `HEAP`, then insert `VALUE` at `KEY`. Returns
      two values: The value and the key of the popped element.
    
    Fails if `HEAP` is empty.

- [function] PEEK HEAP

    Returns two values: The value and the key of the top element of the `HEAP`.

- [function] CHANGE-KEY! HEAP FINGER KEY

    Changes the key of `FINGER` in `HEAP` to `KEY`.

- [function] DELETE! HEAP FINGER

    Remove `FINGER` from `HEAP`.

- [function] EMPTY-HEAP? HEAP

    Returns `T` if `HEAP` is empty, and `NIL` otherwise.
