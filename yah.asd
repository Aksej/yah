;;;; yah.asd

(asdf:defsystem #:yah
  :description "Yet Another Heap"
  :author "Thomas Bartscher <thomas-bartscher@weltraumschlangen.de>"
  :license  "BSD-3"
  :version "0.0.1"
  :depends-on ("mgl-pax")
  :serial t
  :components
  ((:file "package")
   (:file "yah")))
